const express = require("express");
const router = express.Router();
const sql = require("mssql");

const dbConnection = require("../utilities/db");



router.post("/", function (request, response) {

    var FromDate = request.body.FromDate;
    var ToDate = request.body.ToDate;

    try {
        const req = new sql.Request(dbConnection);
       
        req.query(`select * from [adminbox_shanthi].[Monthly_client] where 
        convert(varchar,cast(fld_updatedon as date),103)
           BETWEEN
          convert(varchar,cast('${FromDate}' as date),103)  AND
           convert(varchar,cast('${ToDate}' as date),103) 
        order by fld_updatedon desc`,
            function (err, data) {
                if (err) {
                    console.log("Error while executing the SP - [error] " + err);
                    response.status(404).json({
                        data: err.message
                    });
                } else {
                    response.status(200).json({
                        data: data.recordset
                    });
                }
            });

    } catch (err) {
        response.status(500);
        response.send(err.message);
    }


});

module.exports = router;