const express = require("express");
const router = express.Router();
const sql = require("mssql");

const dbConnection = require("../utilities/db");


router.post("/", function(request, response){
   

    var name = request.body.name;
    var email = request.body.email;
   
    var mobile = request.body.mobile;
    var updatedon = request.body.updatedon;
    var updatedby = request.body.updatedby;
    var termsconditions = request.body.termsconditions;
    var clientStatus = request.body.clientStatus;

console.log(request.body)

    try{
        const req = new sql.Request(dbConnection);
        req.input('name',sql.NVarChar(100), name);
        req.input('email',sql.NVarChar(100), email);
        req.input('mobile',sql.NVarChar(15), mobile);
        req.input('updatedon',sql.NVarChar(200), updatedon);
        req.input('updatedby',sql.Int, updatedby);
        req.input('termsconditions',sql.NVarChar(200), termsconditions);
        req.input('clientStatus',sql.NVarChar(150), clientStatus);
      
       
    
        req.execute("adminbox_shanthi.Add_New_Monthly_Client", function(err, data){
            if(err){
                console.log("Error while executing the SP - [error] " + err);
                response.status(404).json({
                    data:err.message
                });
            }else{
         
                response.status(200).json({
                    data: data.recordset
                });
            }
        });

    }catch (err){
        // console.log(err)
        response.status(500);
        response.send(err.message);
    }


});

module.exports = router;