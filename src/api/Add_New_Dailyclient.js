const express = require("express");
const router = express.Router();
const sql = require("mssql");

const dbConnection = require("../utilities/db");


router.post("/", function(request, response){
   

    var name = request.body.name;
    var username = request.body.username;
   
    var mobile = request.body.mobile;
    var address = request.body.address;
    var landmark = request.body.landmark;
    var area = request.body.area;
    var updatedon = request.body.updatedon;
    var updatedby = request.body.updatedby;
    var agree = request.body.agree;

// console.log(request.body)


    try{
        const req = new sql.Request(dbConnection);
        req.input('name',sql.NVarChar(100), name);
        req.input('username',sql.NVarChar(100), username);
      
        req.input('mobile',sql.NVarChar(15), mobile);
        req.input('address',sql.NVarChar(200), address);
        req.input('landmark',sql.NVarChar(200),landmark);
        req.input('area',sql.NVarChar(200), area);
        req.input('updatedon',sql.NVarChar(200), updatedon);
        req.input('updatedby',sql.Int, updatedby);
        req.input('agree',sql.NVarChar(200), agree);
       
    
        req.execute("adminbox_shanthi.Add_New_DailyClient", function(err, data){
            if(err){
                console.log("Error while executing the SP - [error] " + err);
                response.status(404).json({
                    data:err.message
                });
            }else{
         
                response.status(200).json({
                    data: data.recordset
                });
            }
        });

    }catch (err){
        response.status(500);
        response.send(err.message);
    }


});

module.exports = router;