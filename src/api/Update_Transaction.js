const express = require("express");
const router = express.Router();
const sql = require("mssql");
const dbConnection = require("../utilities/db");


router.post("/", function(request, response){
    var id=request.body.id;
    var accountno = request.body.accountno;
    var installmentno = request.body.installmentno;
    var weight = request.body.weight;
    var status =request.body.status;
    var userid =request.body.userid;
    var transactionNumber=request.body.transactionNumber;

console.log(request.body)

    try{
        const req = new sql.Request(dbConnection);
      
        req.input('id',sql.Int, id);
        req.input('accountno',sql.NVarChar(30), accountno);
        req.input('installmentno',sql.Int, installmentno);
        req.input('weight',sql.NVarChar(100), weight);
        req.input('status',sql.NVarChar(100), status);
        req.input('userid',sql.Int, userid);
        req.input('transactionNumber',sql.NVarChar(100), transactionNumber);
        
        
        req.execute("[adminbox_shanthi].[Update_Transaction_Detail]", function(err, data){
            if(err){
                console.log("Error while executing the SP - [error] " + err);
                response.status(404).json({
                    data:err.message
                });
            }else{
                response.status(200).json({
                    data: data.recordset
                });
            }
        });

    }catch (err){
        response.status(500);
        response.send(err.message);
    }


});

module.exports = router;