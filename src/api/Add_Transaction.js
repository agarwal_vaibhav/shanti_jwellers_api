const express = require("express");
const router = express.Router();
const sql = require("mssql");

const dbConnection = require("../utilities/db");


router.post("/", function(request, response){
   

    var accountno = request.body.accountno;
   
    var paidamount = request.body.paidamount;
    var status = request.body.status;
    var paiddate=request.body.paiddate;
  
    var list = request.body.list;
    
    var chitSchema_id = request.body.chitSchema_id;
    var installment_id = request.body.installment_id;

console.log(request.body)

    try{
        const req = new sql.Request(dbConnection);
        req.input('accountno',sql.NVarChar(30), accountno);
        req.input('paidamount',sql.Decimal(18,0), paidamount);
        req.input('status',sql.NVarChar(100), status);
        req.input('paiddate',sql.NVarChar(200), paiddate);
        req.input('list',sql.NVarChar(100), list);
        req.input('chitSchema_id',sql.Int, chitSchema_id);
        req.input('installment_id',sql.Int, installment_id);
      
       
    
        req.execute("[adminbox_shanthi].[Add_Transaction]", function(err, data){
            if(err){
                console.log("Error while executing the SP - [error] " + err);
                response.status(404).json({
                    data:err.message
                });
            }else{
         
                response.status(200).json({
                    data: data.recordset
                });
            }
        });

    }catch (err){
        // console.log(err)
        response.status(500);
        response.send(err.message);
    }


});

module.exports = router;