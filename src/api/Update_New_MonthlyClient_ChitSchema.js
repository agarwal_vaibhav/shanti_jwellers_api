const express = require("express");
const router = express.Router();
const sql = require("mssql");
const dbConnection = require("../utilities/db");


router.post("/", function(request, response){
   var id=request.body.id;
    var client = request.body.client;
    var accountno = request.body.accountno;
    var chitschema = request.body.chitschema;
    var chitamount = request.body.chitamount;
    var duration = request.body.duration;
   
    var updatedon = request.body.updatedon;
    var updatedby = request.body.updatedby;
    var clientStatus = request.body.clientStatus;
    
   

console.log(request.body)


    try{
        const req = new sql.Request(dbConnection);
        req.input('id',sql.Int, id);
        req.input('client',sql.Int, client);
        req.input('accountno',sql.NVarChar(100), accountno);
        req.input('chitschema',sql.NVarChar(100), chitschema);
        req.input('chitamount',sql.NVarChar(50), chitamount);
        req.input('duration',sql.NVarChar(15),duration);
        req.input('updatedon',sql.NVarChar(200), updatedon);
        req.input('updatedby',sql.Int, updatedby);
        req.input('clientStatus',sql.NVarChar(100), clientStatus);
       
       
    

        req.execute("adminbox_shanthi.Update_New_Monthlyclient_ChitSchema", function(err, data){
            if(err){
                console.log("Error while executing the SP - [error] " + err);
                response.status(404).json({
                    data:err.message
                });
            }else{
                response.status(200).json({
                    data: data.recordset
                });
            }
        });

    }catch (err){
        response.status(500);
        response.send(err.message);
    }


});

module.exports = router;