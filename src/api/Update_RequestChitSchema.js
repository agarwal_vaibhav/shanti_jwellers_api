const express = require("express");
const router = express.Router();
const sql = require("mssql");
const dbConnection = require("../utilities/db");


router.post("/", function(request, response){
    var accountno=request.body.accountno;
    var clientStatus = request.body.clientStatus;
    var client = request.body.client;
    
   

console.log(request.body)


    try{
        const req = new sql.Request(dbConnection);
        
        req.input('accountno',sql.NVarChar(100), accountno);
        req.input('clientStatus',sql.NVarChar(100), clientStatus);
        req.input('client',sql.Int, client);
       
       
    

        req.execute("adminbox_shanthi.Update_registerClientChitSChema", function(err, data){
            if(err){
                console.log("Error while executing the SP - [error] " + err);
                response.status(404).json({
                    data:err.message
                });
            }else{
                response.status(200).json({
                    data: data.recordset
                });
            }
        });

    }catch (err){
        response.status(500);
        response.send(err.message);
    }


});

module.exports = router;