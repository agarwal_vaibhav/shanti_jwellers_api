const express = require("express");
const router = express.Router();
const sql = require("mssql");
// const { body, validationResult } = require('express-validator');
const dbConnection = require("../utilities/db");
// const e = require("express");


router.post("/",

    (req, res) => {


        const { selectfor, fromdate, todate } = req.body
        var fromdatenew = null
        var todatenew = null


        try {
            const request = new sql.Request(dbConnection)


            if (fromdate != null && fromdate != 'Invalid date') {
                fromdatenew = "'".concat(fromdate, "'")
            }
            if (todate != null && todate != 'Invalid date') {
                todatenew = "'".concat(todate, "'")
            }
           
            let value =
            `select * from [adminbox_shanthi].[udv_Transaction_Report]
            where status='Successfully Transfer' AND select_for<= isnull(${selectfor},datediff(day ,'1990-01-01 00:00',getdate())) AND 
            fld_paiddate between isnull(${fromdatenew},'1990-01-01 00:00')  AND 
            isnull(${todatenew},CONVERT(Char(16),getdate(),20))
               order by fld_paiddate desc`





            request.query(`${value}`, (err, data) => {
                if (err) {
                    console.log("Error while executing the SP - [error] " + err);
                    res.status(404).json({
                        data: err.message
                    });
                } else {
                    res.status(200).json({
                        data: data.recordset
                    });
                }
            });

        } catch (err) {
            res.status(500);
            res.send(err.message);
        }


    });

module.exports = router;