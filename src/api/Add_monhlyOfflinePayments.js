const express = require("express");
const router = express.Router();
const sql = require("mssql");

const dbConnection = require("../utilities/db");


router.post("/", function(request, response){
   

    var accountno = request.body.accountno;
    var amounttopay = request.body.amounttopay;
    var paymentdate = request.body.paymentdate;
    var paymentmode = request.body.paymentmode;
    var consolatedweight = request.body.consolatedweight;
    var updatedon = request.body.updatedon;
    var updatedby = request.body.updatedby;


// console.log(request.body)
    try{
        const req = new sql.Request(dbConnection);
    
        req.input('accountno',sql.NVarChar(100), accountno);
        req.input('paymentdate',sql.NVarChar(100), paymentdate);
        req.input('paymentmode',sql.NVarChar(100), paymentmode);
        req.input('amounttopay',sql.Int, amounttopay);
        req.input('updatedon',sql.NVarChar(200), updatedon);
        req.input('consolatedweight',sql.Int, consolatedweight);
        req.input('updatedby',sql.Int, updatedby);

        req.execute("adminbox_shanthi.Add_Monthly_Offline_Payments", function(err, data){
            if(err){
                // console.log("Error while executing the SP - [error] " + err);
                response.status(404).json({
                    data:err.message
                });
            }else{
                 //   console.log(data)
                response.status(200).json({
                    data: data.recordset
                });
            }
        });

    }catch (err){
        response.status(500);
        response.send(err.message);
    }


});

module.exports = router;