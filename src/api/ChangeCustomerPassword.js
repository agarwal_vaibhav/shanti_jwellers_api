const express = require("express");
const router = express.Router();
const sql = require("mssql");
const sha512 = require("js-sha512");

const dbConnection = require("../utilities/db");


router.post("/", function(request, response){
   
    var id = request.body.id;
    var email = request.body.email;
    var password = request.body.password;
    var salt = request.body.salt;
    var hashPassword = sha512(salt+password);
    console.log(request.body.hashPassword)
    console.log(sha512(salt))

    try{
        const req = new sql.Request(dbConnection);
        req.input('id',sql.Int, id);
        req.input('email',sql.NVarChar(100), email);
        req.input('password',sql.NVarChar(200), hashPassword);
        
    
        req.execute("ChangeCustomerPassword", function(err, data){
            if(err){
                console.log("Error while executing the SP - [error] " + err);
                response.status(404).json({
                    data:err.message
                });
            }else{
         
                response.status(200).json({
                    data: data.recordset
                });
            }
        });

    }catch (err){
        response.status(500);
        response.send(err.message);
    }


});

module.exports = router;