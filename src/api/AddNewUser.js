const express = require("express");
const router = express.Router();
const sql = require("mssql");
const sha512 = require("js-sha512");

const dbConnection = require("../utilities/db");


router.post("/", function(request, response){
   
  
   var name =request.body.name;
   var	email =request.body.email;
   var	mobile =request.body.mobile;
   var	usertype =request.body.usertype;
   var	password =request.body.password;

   var	updatedon =request.body.updatedon;
   var updatedby =request.body.updatedby;
    

    
    console.log(request.body)

    try{
        const req = new sql.Request(dbConnection);
    
        req.input('name',sql.NVarChar(100), name);
        req.input('email',sql.NVarChar(100), email);
        req.input('mobile',sql.NVarChar(15), mobile);
        req.input('usertype',sql.NVarChar(100), usertype);
        req.input('updatedon',sql.NVarChar(200), updatedon);
        req.input('updatedby',sql.Int, updatedby);
       
    
        req.execute("[adminbox_shanthi].[Add_New_User]", function(err, data){
            if(err){
                console.log("Error while executing the SP - [error] " + err);
                response.status(404).json({
                    data:err.message
                });
            }else{
         console.log(data.recordset[0].fld_salt)
                var hashPassword = sha512(data.recordset[0].fld_salt+password);

                const req2 = new sql.Request(dbConnection);

                req2.input('password',sql.NVarChar(500), hashPassword);
                req2.input('userid',sql.Int, data.recordset[0].UserId);
               
                req2.execute("[adminbox_shanthi].[Update_New_Admin_User_Password]", function(err2, data2){
                    if(err2){
                        console.log("Error while executing the SP - [error] " + err2);
                        response.status(404).json({
                            data:err2.message
                        });
                    }else{

                        response.status(200).json({
                            data: data.recordset
                        });

                    }
                })
                

                
            }
        });
    }catch (err){
        response.status(500);
        response.send(err.message);
    }


});

module.exports = router;