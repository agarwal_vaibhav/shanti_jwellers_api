const express = require("express");
const router = express.Router();
const sql = require("mssql");

// const dbConnection = require("../../utilities/db2");
var nodemailer = require('nodemailer');
var inlineBase64 = require('nodemailer-plugin-inline-base64');
var smtpTransport = require("nodemailer-smtp-transport");


router.post("/", function(request, response){

    var accountNo = request.body.accountNo;
    var InastallmentNo = request.body.InastallmentNo;
    var Weight = request.body.Weight;
    var email=request.body.email

    // console.log(request.body)

    try{

                    const mailOptions = {
                     
                        from:'ecare@shanthijewellers.com', // sender address
                        to: email, // list of receivers
                        subject: InastallmentNo+ ' Installment Acknowledgement.' , // Subject line
                        html:
                        
                        '<!doctype html>'+
                        '<html>'+
                        '<head>'+
                        '<meta charset="utf-8">'+
                        '<title>Shanthi Jewellers</title>'+
                        '</head>'+                        
                        '<body style="font-family: Gotham,sans-serif">'+
                        '<div class="wrapper" style="padding: 10px;width: 1000px;margin: auto;border: .5px solid;">'+

                            
                        '<div class="container">'+
                            '<img src="http://shanthijewellers.com/images/logo.png" width="180" style="padding-top: 3px"/>'+
                           ' <hr noshade color=#D8D6D6 size=1 style=margin-top:10px; />'+
                           '<p>Dear Shanthi Jewellers,</p>'+'</div>'+
                        '<table style="padding-bottom: 18px">'+'<tr>'+
                        // <td >An user has been made Chit registration through Website, </td>'+'</tr>'+
                           '<tr>'+'<td style="padding-top: 18px">Installment details are as follows :</td>'+'</tr>'+
                           '<tr>'+'<td style="text-align: justify;padding-top: 18px"><b>Account Number</b> <b style="margin-left: 90px;">: </b>'+ ' '+ accountNo +'</td>'+'</tr>'+
                           '<tr>'+'<td style="text-align: justify;padding-top: 18px"><b>Email ID</b>  <b style="margin-left:157px">: </b>'+ ' '+ email+' </td>'+'</tr>'+
                            '<tr>'+'<td style="text-align: justify;padding-top: 18px"><b>Installment Number</b>   <b style="margin-left:70px">: </b>'+ ' '+ InastallmentNo+'</td>'+'</tr>'+
                            '<tr>'+'<td style="text-align: justify;padding-top: 18px"><b>Weight</b> <b style="margin-left:165px">:</b>'+ ' '+ Weight +'</td>'+'</tr>'+
                            '<tr>'+'<td style="text-align: justify;padding-top: 18px">'+'<a href="http://shanthijewellers.com/termsandconditions.pdf">Click here to View Terms and Conidtions</a>'+' </td>'+'</tr>'+
                        '</table>'+
                    '<hr noshade color=#D8D6D6 size=1 style=margin-top:25px;/>'+
                    '</div>'+'</body>'+'</html>'
                         
                         
                    } 

                      transporter.use('compile', inlineBase64({cidPrefix: 'somePrefix_'}));
                      transporter.sendMail(mailOptions, function (err, info) {
                        if(err){
                          response.status(500);
                          console.log(err)
                        }
                        else{
                          response.status(200);
                          console.log(info);
                          response.send(info);
                        }
                         
                     });
                
                   
              

    }catch (err){
        response.status(500);
        response.send(err.message);
    }


});

module.exports = router;


var transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 587,
  secure: false, // use SSL,
  auth: {

    user: 'ecare@shanthijewellers.com',
    pass: 'ecare123'
        
     }
 });


 