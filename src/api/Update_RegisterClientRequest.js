const express = require("express");
const router = express.Router();
const sql = require("mssql");
const dbConnection = require("../utilities/db");


router.post("/", function(request, response){
    var name=request.body.name;
    var clientStatus = request.body.clientStatus;
    var id = request.body.id;
    var userid=request.body.userid;
    
   

console.log(request.body)


    try{
        const req = new sql.Request(dbConnection);
      
        req.input('clientStatus',sql.NVarChar(100), clientStatus);
        req.input('name',sql.NVarChar(100), name);
        req.input('id',sql.Int, id);
        req.input('userid',sql.Int, userid);
        
       
       
    

        req.execute("adminbox_shanthi.Update_registerClient", function(err, data){
            if(err){
                console.log("Error while executing the SP - [error] " + err);
                response.status(404).json({
                    data:err.message
                });
            }else{
                response.status(200).json({
                    data: data.recordset
                });
            }
        });

    }catch (err){
        response.status(500);
        response.send(err.message);
    }


});

module.exports = router;