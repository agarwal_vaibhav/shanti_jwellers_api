const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
var multipart = require('connect-multiparty');


//----------- ADD ------------------


const Add_New_Dailyclient = require("./api/Add_New_Dailyclient")
const Add_New_Dailyclient_chitschema = require("./api/Add_New_Dailyclient_chitschema")
const New_Admin_Login = require("./api/New_Admin_Login")
const Add_monhlyOfflinePayments = require("./api/Add_monhlyOfflinePayments")
const Add_New_Monthlyclient = require("./api/Add_New_Monthlyclient")
const Add_New_MonthlyClient_chitschema = require("./api/Add_New_MonthlyClient_chitschema")
const Get_New_DailyClientList = require("./api/Get_New_DailyClientList")
const Get_New_MonthlyClientList = require("./api/Get_New_MonthlyClientList")
const Delete_New_DailyClient = require("./api/Delete_New_DailyClient")
const Delete_New_MonthlyClient = require("./api/Delete_New_MonthlyClient")
const Get_New_DailyClientSchema = require("./api/Get_New_DailyClientSchema")
const Get_New_MonthlyClientSchema = require("./api/Get_New_MonthlyClientSchema")
const Get_New_SpecificMonthlySchema = require("./api/Get_New_SpecificMonthlySchema")
const Delete_MonthlyClientSpecificSchema = require("./api/Delete_MonthlyClientSpecificSchema")
const Mailer = require("./api/Mailer")
const MailerSecond = require("./api/RegistrationMailer")
const Add_New_CloseAccount = require("./api/Add_New_CloseAccount")
const Get_New_AllClosedAccountList = require("./api/Get_New_AllClosedAccountList")
const Get_MonthlyClientApproved = require("./api/Get_MonthlyClientApproved")
const Get_MonthlyClientPending = require("./api/Get_MonthlyClientPending")


const Update_New_MonthlyClient_ChitSchema = require("./api/Update_New_MonthlyClient_ChitSchema")
const Update_Client_Status = require("./api/Update_Client_Status")
const Update_NewMonthly_Client = require("./api/Update_NewMonthly_Client")
const Update_MonthlyClient_Status = require("./api/Update_MonthlyClient_Status")
const Update_New_MonthlyClient_ChitSchema_Status = require("./api/Update_New_MonthlyClient_ChitSchema_Status")

const Get_New_Client_chitSchema = require("./api/Get_New_Client_chitSchema")
const Get_New_ClientChitSchemaData = require("./api/Get_New_ClientChitSchemaData")
const Get_Approval_ChitData = require("./api/Get_Approval_ChitData")
const Get_Client_ChitScheme = require("./api/Get_Client_ChitScheme")


// ============MenuList=============
const Get_UserMenu = require("./api/Get_UserMenu")
const Get_UserSubMenu = require("./api/Get_UserSubMenu")
const AddNewUser = require("./api/AddNewUser")
const Get_Userlist = require("./api/Get_Userlist")
const GetTransactionlist = require("./api/GetTransactionlist")

const Get_Todaycollection = require("./api/Get_Todaycollection")
const Get_TodayMonthcount = require("./api/Get_TodayMonthcout")

const Get_Thismonthamount = require("./api/Get_Todaymonthamount")

const Get_Thismonthcollection = require("./api/Get_Thismonthcollection")

const Get_ChitSchemaRatio = require("./api/Get_ChitSchemaRatio")

const Get_CustomerCount = require("./api/Get_CustomerCount")


// ============Mailer=====================
const RegistrationMailer = require("./api/RegistrationMailer")
const Get_ApproveData = require("./api/Get_ApproveData")
const Get_AccountList = require("./api/Get_AccountList")


// =================05/04/21==============
const Update_RegisterClientRequest= require("./api/Update_RegisterClientRequest")
const Update_RequestChitSchema = require("./api/Update_RequestChitSchema")
const Customer_Mailer = require("./api/Customer_Mailer")
const Add_Transaction = require("./api/Add_Transaction")
const PaymentApproval_Mailer = require("./api/PaymentApprovalMailer")
const Update_Transaction = require("./api/Update_Transaction")
const Get_AccountListData = require("./api/Get_AccountListData")
const CloseAccount = require("./api/CloseAccount")
const Get_TransactionReport = require("./api/Get_TransactionReport")

// ======06/05/2021==========
const Get_TransactionData = require("./api/Get_TransactionData")


const Update_Customer_Password = require("./api/Update_Customer_Password")

const Get_customerList = require("./api/Get_customerList")
const PaymentSuccess = require("./api/PaymentSuccess")


// =======Forget Paswword========
const Get_SaltByEmailId_ForgotPassword = require("./api/Get_SaltByEmailId_ForgotPassword")
const Mailer_ForgotPassword = require("./api/Mailer_ForgotPassword")
const ChangeCustomerPassword = require("./api/ChangeCustomerPassword")




const app = express();
app.use(bodyParser.json({limit: '50MB', extended: true}));
app.use(bodyParser.urlencoded({limit: '50MB', extended: true }));
app.use(cors());
app.use(multipart({
    maxFieldsSize: '50MB'
}));

var session = require('express-session');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({secret: 'mcg001k',saveUninitialized: true,resave: true}));
app.use(express.static(__dirname + '/'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.set('views', __dirname); 


// ===================================02/Nov/2020===================
app.use("/Add_New_Dailyclient",Add_New_Dailyclient)
app.use("/Add_New_Dailyclient_chitschema",Add_New_Dailyclient_chitschema)
app.use("/New_Admin_Login",New_Admin_Login)
app.use("/Add_monhlyOfflinePayments",Add_monhlyOfflinePayments)
app.use("/Add_New_Monthlyclient", Add_New_Monthlyclient)
app.use("/Add_New_MonthlyClient_chitschema", Add_New_MonthlyClient_chitschema)
app.use("/Get_New_DailyClientList", Get_New_DailyClientList)
app.use("/Get_New_MonthlyClientList", Get_New_MonthlyClientList)
app.use("/Delete_New_DailyClient", Delete_New_DailyClient)
app.use("/Delete_New_MonthlyClient", Delete_New_MonthlyClient)
app.use("/Get_New_DailyClientSchema", Get_New_DailyClientSchema)
app.use("/Get_New_MonthlyClientSchema", Get_New_MonthlyClientSchema)
app.use("/Get_New_SpecificMonthlySchema", Get_New_SpecificMonthlySchema)
app.use("/Delete_MonthlyClientSpecificSchema", Delete_MonthlyClientSpecificSchema)
app.use("/Mailer", Mailer)
app.use("/MailerSecond", MailerSecond)
app.use("/Add_New_CloseAccount", Add_New_CloseAccount)
app.use("/Get_New_AllClosedAccountList", Get_New_AllClosedAccountList)
app.use("/Get_MonthlyClientApproved", Get_MonthlyClientApproved)
app.use("/Get_MonthlyClientPending", Get_MonthlyClientPending)


app.use("/Update_New_MonthlyClient_ChitSchema", Update_New_MonthlyClient_ChitSchema)
app.use("/Update_Client_Status", Update_Client_Status)
app.use("/Update_NewMonthly_Client", Update_NewMonthly_Client)

app.use("/Update_MonthlyClient_Status", Update_MonthlyClient_Status)
app.use("/Update_New_MonthlyClient_ChitSchema_Status", Update_New_MonthlyClient_ChitSchema_Status)

app.use("/Get_New_Client_chitSchema", Get_New_Client_chitSchema)
app.use("/Get_New_ClientChitSchemaData", Get_New_ClientChitSchemaData)

app.use("/Get_Approval_ChitData", Get_Approval_ChitData)

app.use("/Get_Client_ChitScheme", Get_Client_ChitScheme)


// ============MenuList==============
app.use("/Get_UserMenu", Get_UserMenu)
app.use("/Get_UserSubMenu", Get_UserSubMenu)
app.use("/Add_New_User", AddNewUser)
app.use("/Get_Userlist", Get_Userlist)

app.use("/Get_TransactionList", GetTransactionlist)

// =====Dashboard=================

app.use("/Get_Todaycollection", Get_Todaycollection)

app.use("/Get_TodayMonthcount", Get_TodayMonthcount)


app.use("/Get_Thismonthamount", Get_Thismonthamount)

app.use("/Get_Thismonthcollection",Get_Thismonthcollection)
app.use("/Get_ChitSchemaRatio",Get_ChitSchemaRatio)
app.use("/Get_CustomerCount",Get_CustomerCount)

// ===========Registration Mailer=========

app.use("/RegistrationMailer",RegistrationMailer)
app.use("/Get_ApproveData",Get_ApproveData)

app.use("/Update_RegisterClientRequest",Update_RegisterClientRequest)
app.use("/Update_RequestChitSchema",Update_RequestChitSchema)
app.use("/Customer_mailer",Customer_Mailer)
app.use("/Add_Transaction",Add_Transaction)

app.use("/PaymentApproval_Mailer",PaymentApproval_Mailer)
app.use("/Get_AccountList",Get_AccountList)
app.use("/Update_Transaction",Update_Transaction)
app.use("/Get_AccountListData",Get_AccountListData)
app.use("/CloseAccount",CloseAccount)
app.use("/Get_TransactionReport",Get_TransactionReport)


// ==========06/05/2021=========
app.use("/Get_TransactionData",Get_TransactionData)
app.use("/Update_Customer_Password",Update_Customer_Password)
app.use("/Get_customerList",Get_customerList)
app.use("/PaymentSuccess",PaymentSuccess)


// =========Forget Password========
app.use("/Get_SaltByEmailId_ForgotPassword",Get_SaltByEmailId_ForgotPassword)
app.use("/Mailer_ForgotPassword",Mailer_ForgotPassword)
app.use("/ChangeCustomerPassword",ChangeCustomerPassword)




module.exports = app;
